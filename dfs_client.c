/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Some of the code is from Sangtae Ha's example client-server code
 * Date: 12/16/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: dfs_server.c
 * Implements distributed dile system client
 * usage: ./client dfc.conf
 ***************************************************************************************************/

/****************************************************************************************************
*
* Header Files
*
****************************************************************************************************/
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <time.h>
#include <openssl/md5.h>

#define BUFSIZE (4000)
int socks[4];

#pragma pack(1)
struct msg_struct
{
	unsigned int len;
	char data[BUFSIZE];
}msg;
#pragma pack(0)

void signal_handler()
{
	for(int i=0; i<4; i++)
	{
		close(socks[i]);
	}
	exit(0);
}

struct conf_t
{
	char IP_addr[4][20];
	char portno[4][20];
	char server_path[4][20];
	char userName[20];
  	char passWord[20];
};

void xor_function(char *recvBuffer, int bytes, char userPassword[])
{
	int xor_element = strlen(userPassword);
	for(int l = 0; l<bytes; l++)
		recvBuffer[l] ^= xor_element;
}

void user_input(char *cmd, char *file, char *folder)
{
	char buff[50];
	bzero(buff, 50);
	do
	{
		printf("Enter Command\n1. LIST\n2. PUT <FileName>\n3. GET <FileName>\n");
		fgets(buff, 50, stdin);
		sscanf(buff, "%s %s %s", cmd, folder, file);
	}while(strcmp(cmd, "GET") != 0 && strcmp(cmd, "LIST") !=0 && strcmp(cmd, "PUT") != 0 && strcmp(cmd, "EXIT") != 0);
}

int hashValue(FILE *fptr)
{
	char buffer[BUFSIZE];
	unsigned char value[MD5_DIGEST_LENGTH];
	int bytes, ret;
	MD5_CTX md_context;
	MD5_Init(&md_context);
	bzero(buffer, BUFSIZE);
	while((bytes = fread(buffer, 1, BUFSIZE, fptr)) > 0)
		MD5_Update(&md_context, buffer, bytes);
	MD5_Final(value, &md_context);
	int mod=0;
  	for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
        	mod = (mod * 16 + (unsigned int)value[i]) % 4;
	printf("\nMOD = %d\n", mod);
	return mod;
}

void sendFiles(char *usrFile, char *usrFolder, char *password, int offset, int sSize, int socks, int part_no)
{
	char message[BUFSIZE];
	bzero(message, BUFSIZE);
	if(socks == -1) return;
	FILE *fptr = fopen(usrFile, "r");
	if(!fptr)
	{
		return;
	}
	fseek(fptr, offset, SEEK_SET);
	sprintf(message, "PUT %s %s %d", usrFolder, usrFile, part_no);
	send(socks, message, BUFSIZE, 0);
	send(socks, &sSize, sizeof(int), 0);
	bzero(message, BUFSIZE);
	int bytes, readSize;
	if(BUFSIZE > sSize)
	{
		bytes = fread(msg.data, 1, sSize, fptr);
		msg.len = bytes;
		xor_function(msg.data, sSize, password);
		send(socks, &msg, sizeof(msg), 0);
	}
	else	
	{
		while(sSize > BUFSIZE)
		{
			bytes = fread(msg.data, 1, BUFSIZE, fptr);
			sSize -= BUFSIZE; 
			msg.len = bytes;
			xor_function(msg.data, msg.len, password);
			send(socks, &msg, sizeof(msg), 0);
			bzero(&msg, sizeof(msg));
		}
		bytes = fread(msg.data, 1, sSize, fptr);
		xor_function(msg.data, bytes, password);
		msg.len = bytes;
		send(socks, &msg, sizeof(msg), 0);
	}
	fclose(fptr);
}

int get_function(int socks[], char *usrFile, char *usrFolder, char *passWord)
{
	if(!usrFile || !usrFolder || !passWord)
	{
		printf("NULL received\n");
		for(int k=0; k<4; k++)
			send(socks[k], "EXIT exit exit 1", BUFSIZE, 0);
		return -1;
	}
	char message[BUFSIZE];
	bool file_part[4] = {false};
	bzero(message, BUFSIZE);
	char fName[] = "Get";
	strcat(fName, usrFile);
	int part_no, bytes;
	FILE *fptr = fopen(fName, "w");
	if(!fptr)
	{
		printf("NULLPTR\n");
		for(int k=0; k<4; k++)
			send(socks[k], "EXIT exit exit 1", BUFSIZE, 0);
		return 0;
	}
	for(part_no=1; part_no<5; part_no++)
	{
		int i=0;
		bzero(message, BUFSIZE);
		sprintf(message, "GET %s %s %d", usrFolder, usrFile, part_no);
		for(i=0; i<4; i++)
		{
			size_t size=0;
			if(socks[i] != -1)
			{
				send(socks[i], message, BUFSIZE, 0);
				nanosleep((const struct timespec[]){{0, 200000000L}}, NULL);
				recv(socks[i], &size, sizeof(size), 0);
				if(size > 0)
				{	
					char *ptr = (char *)malloc(size+1);
					if(ptr)
					{
						printf("S: %d F: %d size: %ld\n", i+1, part_no, size);
						bytes = recv(socks[i], ptr, size, 0);
						xor_function(ptr, bytes, passWord);
						fwrite(ptr, 1, bytes, fptr);
						file_part[part_no-1] = true;
						free(ptr);
					}
					/*fptr = fopen(fName, "a");
					while(size > 0)
					{
						bytes = recv(socks[i], &msg, sizeof(msg), 0);
						size = size - msg.len;
						xor_function(msg.data, bytes, passWord);
						fwrite(msg.data, 1, msg.len, fptr);
						bzero(&msg, sizeof(msg));
					}*/
					//fclose(fptr);
					i=6;
				}
			}
		}
	}
	fclose(fptr);
	if(file_part[0] != true || file_part[1]!=true || file_part[2]!=true ||file_part[3]!=true)
	{
		printf("Cannot get whole file\n");
		remove(fName);
		return 0;
	}
}

int put_function(int socks[], char *usrFile, char *usrFolder, char *passWord )
{
	int k;
	FILE *fptr = fopen(usrFile, "r");
	if(!fptr)
	{
		printf("File Open Error\n");
		for(int k=0; k<4; k++)
			send(socks[k], "EXIT exit exit 1", BUFSIZE, 0);
		return 0;
	}
	fseek(fptr, 0, SEEK_END);
	int sz = ftell(fptr);
	rewind(fptr);
	fclose(fptr);
	int sSize = sz / 4;
	int lSize = (sz - (sSize*3));
	int hash_value = hashValue(fptr);
	if(hash_value == 0)
	{
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[0], 1);
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[3], 1);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[0], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[1], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[1], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[2], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[2], 4);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[3], 4);
	}
	else if(hash_value == 1)
	{
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[0], 1);
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[1], 1);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[1], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[2], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[2], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[3], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[0], 4);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[3], 4);
	}
	else if(hash_value == 2)
	{
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[1], 1);
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[2], 1);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[2], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[3], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[3], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[0], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[0], 4);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[1], 4);
	}
	else if(hash_value == 3)
	{
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[2], 1);
		sendFiles(usrFile, usrFolder, passWord, 0, sSize, socks[3], 1);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[3], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize, sSize, socks[0], 2);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[0], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*2, sSize, socks[1], 3);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[1], 4);
		sendFiles(usrFile, usrFolder, passWord, sSize*3, lSize, socks[2], 4);
	}
}

int list_function(int socks[], char *usrFolder)
{
	char message[BUFSIZE];
	char ls_name[4][100][50];
	char name[4][50];
	int num[4], k, j;
	char *str, *no;
	FILE *fptr = fopen("fileName.txt", "w");
	for (int q=0; q<4; q++)
	{	
		if(socks[q] != -1)
		{
			bzero(message, BUFSIZE);
			sprintf(message, "LIST %s", usrFolder);
			send(socks[q], message, BUFSIZE, 0);
			bzero(message, BUFSIZE);
			int count;
			recv(socks[q], &count, sizeof(int), 0);
			if(count > 0)
			{	num[q] = count;
				while(count > 0)
				{
					count--;
					recv(socks[q], message, BUFSIZE, 0);
					sprintf(ls_name[q][count], "%s\n", message);
					fwrite(ls_name[q][count], 1, strlen(ls_name[q][count]), fptr);
					//printf("Server %d: %s\n", q, message);
					bzero(message, BUFSIZE);
				}
			}
			else
				printf("Server %d: %s Folder Doesn't exist\n", q, usrFolder);
		}
	}	
	fclose(fptr);
	bzero(message, BUFSIZE);
	sprintf(message, "%s", "sort fileName.txt | uniq >filename.txt");
	system(message);
	int number=0, ft;
	char past[BUFSIZE], pr[100], pa[100];
	bzero(message, BUFSIZE);
	FILE *lp = fopen("filename.txt", "r");
	char *tmp;
	printf("\n\n***************FILES IN SERVER*********************\n\n");
	while(fgets(message, BUFSIZE, lp) > 0)
	{
		ft = ftell(lp);
		snprintf(pr, strlen(message)-2, "%s", message);
		fgets(past, BUFSIZE, lp);
		snprintf(pa, strlen(past)-2, "%s", past);
		if(strcmp(pr, pa) != 0)
		{
			fseek(lp, ft, SEEK_SET);
			printf("%s [incomplete]\n", pr);
			continue;	
		}		
		ft = ftell(lp);
		bzero(past, BUFSIZE); bzero(pa, 100);
		fgets(past, BUFSIZE, lp);
		snprintf(pa, strlen(past)-2, "%s", past);
		if(strcmp(pr, pa) != 0)
		{
			fseek(lp, ft, SEEK_SET);
			printf("%s [incomplete]\n", pr);	
			continue;
		}
		ft = ftell(lp);
		bzero(past, BUFSIZE); bzero(pa, 100);
		fgets(past, BUFSIZE, lp);
		snprintf(pa, strlen(past)-2, "%s", past);
		if(strcmp(pr, pa) != 0)
		{
			fseek(lp, ft, SEEK_SET);
			printf("%s [incomplete]\n", pr);	
			continue;
		}
		printf("%s\n", pr);
		
	}
	printf("\n\n***************FILES IN SERVER*********************\n\n");
	fclose(lp);
}

int getConf_data(struct conf_t *ptr, char *fileName)
{
	int i =0;
	char fLine[BUFSIZE], tempBuff[BUFSIZE], str[50], ser[50], *temp, *temp1;
	FILE *fptr = fopen(fileName, "r");
	if(!fptr)
	{
		printf("Conf File opening error\n");
		return -1;
	}
	int size = 0;
	while((fgets(fLine,BUFSIZE,fptr))!=NULL)
	{
		strcpy(tempBuff, fLine);
		temp = strtok(fLine, ":");
		if(strncmp(temp, "Username", 8) == 0)
		{
			strcpy(ptr->userName, (strtok(NULL, "\n")+1));
		}
		else if(strncmp(temp, "Password", 8) == 0)
		{
			strcpy(ptr->passWord, (strtok(NULL, "\n")+1));
		}
		else if(strncmp(temp, "Server", 6) == 0)
		{
			sscanf(tempBuff, "%s %s %s", ser, ptr->server_path[i], str);
			temp1 = strtok(str, ":");
			strncpy(ptr->IP_addr[i], temp1, strlen(temp1));
			strcpy(ptr->portno[i], strtok(NULL, "\n"));
			i++;
		}
		bzero(fLine, BUFSIZE);
		bzero(tempBuff, BUFSIZE);
	}
	fclose(fptr);
	fflush(stdin);
	return 0;
}

int main(int argc, char *argv[]) 
{
	
	// int socket_desc, client_sock , c , read_size;
	struct sockaddr_in client;     
	struct timeval timeout;
	struct conf_t configStruct;
	char message[BUFSIZE];
	char buffer[BUFSIZE];
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	int nbytes, i, status, port_n;
	char confFile[50], usrFile[50], usrCmd[50], usrFolder[50];
	
	// Make sure user passes in a .conf file
	if (argc != 2) 
	{
		printf("USAGE: ./client dfc.conf\n");
		return -1;
	}
	bzero(confFile, 50);
	//snprintf(confFile, strlen(argv[1])+1, "%s", argv[1]);
	strncpy(confFile, argv[1], strlen(argv[1])+1);
	
	status = getConf_data(&configStruct, confFile); 
	//snprintf(message, 40, "%s %s", configStruct.userName, configStruct.passWord);
	strncpy(message, configStruct.userName, strlen(configStruct.userName));
	strcat(message, " ");
	strncat(message, configStruct.passWord, strlen(configStruct.passWord));
	if(status<0)
	{
		printf("Cannot read Config File\n");
		return 0;
	}
	//Create sockets
	signal(SIGINT, signal_handler);
	while(1)
	{
		bzero(usrCmd, 50); bzero(usrFile, 50); bzero(usrFolder, 50);
		user_input(usrCmd, usrFile, usrFolder);
		for(i = 0; i<4; i++)
		{
	    		if((socks[i] = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
			{
	        		printf("Error creating socket\n");
				socks[i] = -1;
	        	}
			if (setsockopt (socks[i], SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
	        		    sizeof(timeout)) < 0)
	        			perror("setsockopt failed: ");
		}
		for(i=0; i<4; i++)
		{
	        	client.sin_family = AF_INET;
			port_n = atoi(configStruct.portno[i]);
	        	client.sin_port = htons(port_n);
	        	client.sin_addr.s_addr = inet_addr(configStruct.IP_addr[i]);
	                		//Connect the socket to the server
	        	if (connect(socks[i], (struct sockaddr *)&client, sizeof(client)) < 0) 
			{
				printf("Error connecting socket\n ");
				socks[i] = -1;
	        	}
		}
		for (i=0; i<=3; i++)
		{
			if(socks[i] != -1)
			{
				send(socks[i], message, BUFSIZE, 0);
				int match;
	        		nbytes = recv(socks[i], &match, sizeof(match), 0);
	        		if(nbytes == 0) 
				{
	        			printf("Timeout occurred for server %d. Server not available.\n", i);	
					close(socks[i]);
					continue;
				}
				if(match != 1)
				{ 
					printf("Invalid Username/Password for server %d. Please Try Again\n", i);
					send(socks[i], "EXIT exit exit 1", BUFSIZE, 0);	
					close(socks[i]);
					socks[i] = -1;
					continue;
				}
			}
		}
		if(socks[0]== -1 && socks[1]==-1 && socks[2]==-1 && socks[3]==-1)
		{
			printf("All authentication failed. Try again\n");
			return 0;
		}
		if(strcmp(usrCmd, "EXIT")==0)
		{
			sprintf(message, "EXIT exit exit 1");
			for(i=0; i<4; i++)
			{
				if(socks[i] != -1)
					send(socks[i], message, BUFSIZE, 0);
			}
			for(i=0; i<4; i++)
			close(socks[i]);
			printf("EXITING.....................\n");
			exit(0);
		}
		else if(strcmp(usrCmd, "GET") == 0)
		{
			get_function(socks, usrFile, usrFolder, configStruct.passWord);
		}
		else if(strcmp(usrCmd, "PUT") == 0)
		{
			put_function(socks, usrFile, usrFolder, configStruct.passWord);
		}
		else if(strcmp(usrCmd, "LIST") == 0)
		{
			list_function(socks, usrFolder);
		}
		else
		{
			printf("NOT DETECTED\n");
		}	
		for(int k=0; k<4; k++)
		{
			if(socks[k] != -1)
			{
				send(socks[k], "EXIT exit exit 1", BUFSIZE, 0);
				close(socks[k]);
			}
		}
	}
}
