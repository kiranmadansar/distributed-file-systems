/*****************************************************************************************************
 *
 * Author: Kiran Hegde
 * Some of the code is from Sangtae Ha's example client-server code
 * Date: 12/16/2018
 * Tools: Vim editor and GCC compiler
 * 
 * File: dfs_server.c
 * Implements distributed dile system server
 * usage: ./server DFS* <port>
 ***************************************************************************************************/

/****************************************************************************************************
*
* Header Files
*
****************************************************************************************************/
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/dir.h>

#define BUFSIZE (4000)
#define MAXFILES (25)
#define MAXCHAR (100)

int sockfd, client_sock;
char folderName[BUFSIZE];
char *conf_file = "dfs.conf";

#pragma pack(1)
struct msg_struct
{
	unsigned int len;
	char data[BUFSIZE];
}msg;
#pragma pack(0)

void signal_handler()
{
	close(sockfd);
	close(client_sock);
	printf("\nSignal Handler\n");
	exit(0);
}

int file_select(const struct direct *entry)
{
	if ((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))
	return (0);
	else
	return (1);
}


int list_function(int client_sock, char fName[])
{
	int count, m;
	struct direct **files;
	char fileName[BUFSIZE];
	bzero(fileName, BUFSIZE);
	count = scandir(fName, &files, file_select, alphasort);
	if(count <= 0)
	{
		printf("No file exists\n");
		strcpy(fileName, "No file exists");
		send(client_sock, &count, BUFSIZE, 0);
		return 0;
	}
	send(client_sock, &count, sizeof(count), 0);
	for (m=0; m<count; m++)
	{
		sprintf(fileName, "%s",files[m]->d_name);
		printf("%s\n", fileName);
		send(client_sock, fileName, BUFSIZE, 0);
		bzero(fileName, BUFSIZE);
	}
	return 0;
}


int get_function(int client_sock, char fName[])
{
	int bytes=0;
	size_t sz=0;
	FILE *fptr = fopen(fName, "r");
	if(!fptr)
	{
		printf("File Open Error\n");
		sz = 0;
		send(client_sock, &sz, sizeof(sz), 0);
		return -1;
	}
	fseek(fptr, 0L, SEEK_END);
    	sz = ftell(fptr);
    	rewind(fptr);
	send(client_sock, &sz, sizeof(sz), 0);
	bzero(&msg, sizeof(msg));
	char *ptr = (char *)malloc(sz+1);
	if(ptr)
	{
		printf("size: %ld\n", sz);
		bytes = fread(ptr, 1, sz, fptr);
		send(client_sock, ptr, bytes, 0);
		free(ptr);
	}
	/*if(BUFSIZE > sz)
	{
		bytes = fread(msg.data, 1, BUFSIZE, fptr);
		msg.len = bytes;
		send(client_sock, &msg, sizeof(msg), 0);
	}
	else
	{
		while(sz>BUFSIZE)
		{
			bytes = fread(msg.data, 1, BUFSIZE, fptr);
			sz = sz-BUFSIZE;
			msg.len = bytes;
			send(client_sock, &msg, sizeof(msg), 0);
			bzero(&msg, sizeof(msg));
		}
		bytes = fread(msg.data, 1, sz, fptr);
		msg.len = bytes;
		send(client_sock, &msg, sizeof(msg), 0);
	}*/
	fclose(fptr);
	return 0;
}

int put_function(int client_sock, char fName[], char* subFolder, char *folderName, char *userName)
{
	char recvBuffer[BUFSIZE], path[100];
	int bytes;
	sprintf(path, "%s/%s/%s", folderName, userName, subFolder);
	struct stat st = {};
    	if(stat(path, &st) == -1)
    	{
        	mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}
	FILE *fptr = fopen(fName, "w");
	if(!fptr)
	{
		printf("File Open Error\n");
		return -1;
	}
	int size;
	if((bytes = recv(client_sock, &size, sizeof(size), 0)) < 0)
		return -1;
	bzero(recvBuffer, BUFSIZE);
	while(size>0)
	{
		bytes = recv(client_sock, &msg, sizeof(msg), 0);
		size = size - msg.len;
		fwrite(msg.data, 1, msg.len, fptr);
		bzero(&msg, sizeof(msg));
	}
	fclose(fptr);
	return 0;
}

int server_authentication(char userName[], char userPassword[])
{
	char fLine[BUFSIZE], user[MAXCHAR], pass[MAXCHAR], fName[BUFSIZE];
	bzero(fLine, BUFSIZE);
	int match = 0;
	FILE *fptr = fopen(conf_file, "r");
	bzero(fLine, BUFSIZE);
	if(!fptr)
	{	
		return (-1);
	}
	else
	{
		while(fgets(fLine, BUFSIZE, fptr) != NULL)
		{
			sscanf(fLine, "%s %s", user, pass);
			if( strcmp( user, userName ) == 0 && strcmp( pass, userPassword ) == 0 )
			{
				strcat(fName, folderName);
				strcat(fName,"/");
				strcat(fName, user);
				struct stat st = {};
    				if(stat(fName, &st) == -1)
    				{
        				mkdir(fName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
				}
				match = 1;
				send(client_sock, &match, sizeof(match), 0);
				fclose(fptr);
				return 0;
			}
			bzero(fLine, BUFSIZE);
		}
		match = 0;
		send(client_sock, &match, sizeof(match), 0);
		fclose(fptr);
		return -1;
	}
}

int fork_function(int client_sock)
{
	int status=-1, fNumbr;
	char userName[MAXCHAR];
	char userPassword[MAXCHAR];
	char recvBuffer[BUFSIZE];
	char usrCmd[BUFSIZE];
	char usrFile[BUFSIZE];
	char fileNmbr[BUFSIZE];
	char fName[BUFSIZE], subFolder[BUFSIZE];

	recv(client_sock, recvBuffer, BUFSIZE, 0);
	sscanf(recvBuffer, "%s %s", userName, userPassword);
	status = server_authentication( userName, userPassword );
	if(status<0)
	{
		printf("Invalid Username/Password. Please try again\n");
		return -1;
	}
	printf("Password matched\n");
	while(1)
	{
		bzero(recvBuffer, BUFSIZE);
		bzero(usrCmd, BUFSIZE);
		bzero(usrFile, BUFSIZE);
		bzero(fileNmbr, BUFSIZE);
		bzero(subFolder, BUFSIZE);
		status = -1;
		if((status = recv(client_sock, recvBuffer, BUFSIZE, 0))<0)
		{
			printf("Receive Failed\n");
			return -1;
		}
		sscanf(recvBuffer, "%s %s %s %s", usrCmd, subFolder, usrFile, fileNmbr);
		fNumbr = atoi(fileNmbr);
		if(strcmp(usrCmd, "EXIT") == 0)
			return -1;
		if(strcmp(usrCmd, "PUT") == 0 || strcmp(usrCmd, "put") == 0)	
		{
			printf("%s %u\n", usrFile, fNumbr);
			snprintf(fName, BUFSIZE, "%s/%s/%s/%s.%u", folderName, userName, subFolder, usrFile, fNumbr);
			status = -1;
			status = put_function(client_sock, fName, subFolder, folderName, userName);
			if(status<0)
			{
				printf("Error in PUT command\n");
				continue;
			}		
			printf("PUT succeded\n");
		}
		if(strcmp(usrCmd, "GET") == 0 || strcmp(usrCmd, "get") == 0)
		{
			snprintf(fName, BUFSIZE, "%s/%s/%s/%s.%u", folderName, userName, subFolder, usrFile, fNumbr);
			status = get_function(client_sock, fName);
			if(status < 0)
			{
				printf("Error in Get Command\n");
				continue;
			}
			printf("GET succeded\n");
		}
		if(strcmp(usrCmd, "LIST") == 0 || strcmp(usrCmd, "list") == 0)
		{
			snprintf(fName, BUFSIZE, "%s/%s/%s/", folderName, userName, subFolder);
			status = list_function(client_sock, fName);
			if(status < 0)
			{
				printf("Error in LIST Command\n");
				return -1;
			}
			printf("LIST succeded\n");
		}
		
	}		
}

int main(int argc, char **argv) 
{
	int portno; /* port to listen on */
	int clientlen; /* byte size of client's address */
	struct sockaddr_in serveraddr; /* server's addr */
	struct sockaddr_in clientaddr; /* client addr */
	struct hostent *hostp; /* client host info */
	char *hostaddrp; /* dotted decimal host addr string */
	int optval; /* flag value for setsockopt */
	int ret, count = 0; /* message byte size */
	pid_t pid;
	/* 
	* check command line arguments 
	*/
	if (argc != 3) 
	{
    		fprintf(stderr, "USAGE: %s <FOLDER> <PORT>\n", argv[0]);
		exit(0);
	}
	snprintf(folderName, BUFSIZE, "%s", argv[1]);
	portno = atoi(argv[2]);
	
	struct stat st = {};
    	if(stat(folderName, &st) == -1)
    	{
        	mkdir(folderName, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}
	/* 
	* socket: create the parent socket 
	*/
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		printf("ERROR opening socket");
	int opt=1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt));
	/*
	* build the server's Internet address
	*/
	bzero((char *) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short)portno);

	/* 
	* bind: associate the parent socket with a port 
	*/
	if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
		printf("ERROR on binding");

	/* 
	* main loop: wait for a datagram, then echo it
	*/
	listen(sockfd, 5);
	clientlen = sizeof(serveraddr);
	signal(SIGINT, signal_handler);
	while(1)
	{
		if((client_sock = accept(sockfd, (struct sockaddr *)&serveraddr, (socklen_t *)&clientlen)) < 0)
		{
			printf("Accept failed\n");
			return 1;
		}
		pid = fork();
		if (pid<0) return -1;
		else if (pid == 0)
		{
			close(sockfd);
			ret = fork_function(client_sock);
		        shutdown(client_sock, SHUT_RDWR);
			close(client_sock);
			exit(0);	
		}
		else
		{
			close(client_sock);
		}

	}
	return 0;
}

