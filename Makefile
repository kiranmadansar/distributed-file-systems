CC=gcc

.PHONY: all
all:
	$(CC) dfs_client.c -lcrypto -lssl -o client
	$(CC) dfs_server.c -o server

.PHONY: clean
clean:
	rm -f client server
