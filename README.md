﻿Date: 12/16/2018
Programming Assignment 4 (CSCI 5273) - Distributed File System
Submitted by Kiran Narayana Hegde


I have implemented a distributed file system server and client which supports multiple connection. I have used fork() to create child process.


Only “GET <filename>", "PUT <filename> and LIST" requests are supported. 

Client divides the given file into 4 pieces and sends it to server using TCP protocol. 
Every Server contains two parts of the file for reliability through redundancy. 
Username and Password Authentication is needed for the application to work. It can be inputted using .conf files.
XOR Data encryption is done with Password as key. 
Traffic optimization is done for GET command.



How to build and use the application ?


1. There is a directory named ‘kihe6592_PA4’ in the submitted .tar file. Untar it
2. Go to directory and use the following make command to build client application
3. In terminal, run the server application using the following commands

```sh
$ make
$ ./server DFS1 <port_no>
$ ./client dfc.conf
```

4. Use the above commands to communicate between server and client 
